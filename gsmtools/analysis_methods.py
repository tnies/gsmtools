#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains several analysis nethods for genome scale modesl
"""

import numpy as np
import pandas as pd
from cobra.sampling import sample
from .utils import get_exchange_reactions
from .element_utils import get_e_composition


__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


def robustness_analysis(model,input_flux,constraint_range):
    """
    Calculate optimized fluxes for a range of influxes
    
    Input
    -----
    model:            Cobrapy genome-scale model 
    input_flux:       Name of exchange flux that should be varied [string]
    constraint_range: Range of influxes [np.array]
    
    Output
    ------
    Pandas Datframes with optimized fluxes (columns) for each influx value 
    (rows)
    
    """
    fluxes = []
    with model as m:
        for i in constraint_range:
            m.reactions.get_by_id(input_flux).bounds = (-i,-i)
            sol = m.optimize()
            fluxes.append(sol.fluxes)
    return pd.DataFrame(fluxes)



def flux_cone_middle_point(model,number_of_samples=100):
    """
    Calculates a centroid of the flux cone by sampling it uniformily. 
    As default the OptGp algorithm is choosen for sampling
    
    Input
    -----
    model:              Cobrapy genome-scale model 
    number_of_samples:  Number of samples [integer]
    
    Output
    ------
    middle: Means of sampled fluxes [np.array]
    """
    flux_samples = sample(model,number_of_samples,method='optgp')
    middle = np.mean(flux_samples,axis=0)
    return middle



def get_proto_metabolism(model,carbon_sources,lower_bound=-10,number_of_sample=20000):
    """
    Calculates prototypic metabolism by flux_sampling.
    
    Input
    -----
    model:              Cobrapy genome-scale model 
    number_of_samples:  Number of samples [integer]
    lower_bound:        Influx bound
    
    Output
    ------
    numpy array

    """
    proto_metabolism = []
    for i in carbon_sources:
        with model as m:
            m.reactions.get_by_id(i).bounds = (lower_bound,lower_bound)
            flux_samples = flux_cone_middle_point(m,number_of_sample)[carbon_sources]
            print(flux_samples)
            yields = flux_samples/flux_samples[i]
            print(yields)
            yields[i] = 0
            proto_metabolism.append(yields/sum(abs(yields)))
    return proto_metabolism



def get_proto_metabolism_by_optimization(model,carbon_sources,
                                         biomass_reaction,source_bounds = (-10,0),f=0.1):
    """
    Calculates proto metabolism by optimization. Basically what is the maximum
    exchange when a carbon source is specified and the biomass must be a fraction
    of the maximal biomass flux under the same conditions
    
    Input
    -----
    model:              cobrapy model without any exchange flux
    carbon_sources:     list of carbon sources [string list]
    biomass_reaction:   id of used biomass reaction[string]
    source_bounds:      ub and lb of assumed carbon source influx [tuple]
    
    Output
    ------
    matrix with maximal exchange flux for carbon source specified in
    carbon_sources under a constraint biomass (max_bio*f)
    
    """
    proto_met = []
    for i in carbon_sources:
        with model as m:
            m.reactions.get_by_id(i).bounds = source_bounds
            sol_bio = float(m.optimize()[biomass_reaction])
            print(sol_bio)
            if f != False :
                m.reactions.get_by_id(biomass_reaction).bounds = (f*sol_bio,f*sol_bio)
            res_dict = {i:0}
            for j in carbon_sources:
                if j != i:
                    res_dict[j] = get_maximum_yield(m,j)[j]
            proto_met.append(res_dict)
            m.reactions.get_by_id(i).bounds = (0,0)
            m.reactions.get_by_id(biomass_reaction).bounds = (-1000,1000)
    proto_met = pd.DataFrame(proto_met)
    proto_met.index = carbon_sources
    return proto_met[proto_met.index]
           
    

def get_maximum_yield(model,exchange_reaction,exchange_bounds = (0,1000)):
    """
    Calculates maximum product yield capacity (MPY) of an organism
    
    max z=v_EX_P_e [MPY]
    s.t: sum_j S_ij*vj = 0 for all i in I
    LB_j <= v_j <= UB_j for all j in J
    v_j are real numbers 
    
    Input
    -----
    model:               cobrapy model
    exchange_reaction:   Product exchange reaction that should be maximized
    
    Output
    ------
    sol: cobra sol object
    """
    with model as m:
        m.reactions.get_by_id(exchange_reaction).bounds = exchange_bounds
        m.objective = {m.reactions.get_by_id(exchange_reaction): 1}
        sol = m.optimize()
    return sol



def get_trade_off(model,exchange_reaction,biomass_reaction,step_size = 0.05):
    """
    Calculates the trade off between two fluxes
    
    max (and min) z = v_EX_P_e
    s.t: sum_j S_ij*vj = 0 for all i in I
    LB_j <= v_j <= UB_j for all j in J
    v_biomass = f*v^{max,WT}_biomass where 0 <= f <= 1
    v_j are real numbers 
    
    Input
    -----
    model:              cobrapy genome-scale model
    exchange_reaction:  Product exchange reaction that should be maximized,
    step_size:          Number of steps in interval 0 <= f <= 1
    Output
    ------
    Pandas Dataframe containing f, bio*f, maximized v_EX_P_e, minimized v_EX_P_e
    
    """
    sol_bio = float(model.optimize()[biomass_reaction])
    res = []
    steps = np.arange(0,1+step_size,step_size) 
    for i in steps:
        res_dict = {'f':i,'bio_frac':i*sol_bio}
        with model as m:
            m.reactions.get_by_id(biomass_reaction).bounds = (res_dict['bio_frac'],res_dict['bio_frac'])
            for j in ['min','max']:
                m.objective_direction = j
                res_dict[j] = get_maximum_yield(m,exchange_reaction)[exchange_reaction]
        res.append(res_dict)
    return pd.DataFrame(res)



def get_carbon_fraction_in_anabolism(model,exchange_reaction,exchange_bounds=(-10,0)):
    """
    Calculates the carbon fraction of the carbon source that is channelled into
    anabolism
    """
    with model as m:
        metabolite = exchange_reaction.split('EX_')[1]
        e_dict = get_e_composition(m)
        idx_ex = get_exchange_reactions([i.id for i in m.reactions])
        m.reactions.get_by_id(exchange_reaction).bounds = exchange_bounds
        sol = m.optimize()
        sol_ex = sol.fluxes[idx_ex]
        sol_ex = sol_ex[sol_ex != 0]/sol_ex[exchange_reaction] #actuall *-1
        elements = pd.DataFrame({i:e_dict[i.split('EX_')[1]] for i in sol_ex.index})
        C_in_ana = np.sum(elements*sol_ex,axis=1)
        return C_in_ana['C']/m.metabolites.get_by_id(metabolite).elements['C']
    
    
def estimate_C_matrix(model,carbon_sources,biomass_reaction,source_bounds=(-10,0), normalize=1, number_species = 1, mu=1, s=0.3, org_C=False):
    """
    Calculates the C matrix in the extended MacArthur's consumer resource model
    """
    c_org = []
    C_matrix = []
    for i in carbon_sources:
        with model as m:
            m.reactions.get_by_id(i).bounds = source_bounds
            sol = m.optimize()
            if sol.status == 'optimal':
                c_org.append(sol[biomass_reaction])
            else:
                c_org.append(0)
    if org_C==False:
        cnt = 0
        while cnt < number_species:
            C_matrix.append(c_org*np.random.normal(mu,s,size=len(carbon_sources)))
            cnt +=1
        C_matrix = np.array(C_matrix).T/np.sum(np.array(C_matrix)*normalize,axis=1)
        return c_org, C_matrix.T
    else:
        return c_org
    
    
def community_C_matrix(models,carbon_sources,biomass_reaction,source_bounds=(-10,0), normalize=1):
    """
    Calculates the C matrix in the extended MacArthur's consumer resource model
    for a microbial community
    """
    C_matrix = []
    for i in range(len(models)):
        c = estimate_C_matrix(models[i],carbon_sources,biomass_reaction[i],source_bounds=(-10,0),org_C=True)
        C_matrix.append(c)
    C_matrix = np.array(C_matrix).T/np.sum(np.array(C_matrix)*normalize,axis=1)
    return C_matrix.T
    
        

            


__all__=['robustness_analysis','flux_cone_middle_point','get_proto_metabolism',
         'get_proto_metabolism_by_optimization','get_maximum_yield','get_trade_off',
         'get_carbon_fraction_in_anabolism','estimate_C_matrix', 'community_C_matrix']