#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This file molecular weights of elements and thermodynamic constants
"""

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


#contains molecular weights of chemical elements in g/mol or u
molecular_weights = {'H':1.0079,'Li':6.94,'Na':22.990,'K':39.098,'Rb':85.468,
                     'Cs':132.91,'Fr':223.02,'Be':9.0122,'Mg':24.305,'Ca':40.078,
                     'Sr':87.62,'Ba':137.33,'Ra':226.03,'Sc':44.956,'Y':88.906,
                     'La':138.91,'Ac':227.03,'Ti':47.867,'Zr':91.224,'Hf':178.49,
                     'Rf':267,'V':50.942,'Nb':92.902,'Ta':180.95,'Db':268,'Cr':51.996,
                     'Mo':95.96,'W':183.84,'Sg':271,'Mn':54.938,'Tc':97.907,'Re':186.21,
                     'Bh':267,'Fe':55.845,'Ru':101.07,'Os':190.23,'Hs':277,'Co':58.693,
                     'Rh':102.91,'Ir':192.22,'Mt':278,'Ni':58.693,'Pd':106.42,
                     'Pt':195.08,'Ds':281,'Cu':63.546,'Ag':107.87,'Au':196.97,
                     'Rg':280,'Zn':65.38,'Cd':112.41,'Hg':200.59,'Cn':285,
                     'B':10.811,'Al':26.982,'Ga':69.733,'In':114.82,'Tl':204.38,
                     'Uut':284,'C':12.011,'Si':28.086,'Ge':72.64,'Sn':118.71,
                     'Pb':207.2,'Uuq':290,'N':14.007,'P':30.974,'As':74.922,'Sb':121.76,
                     'Bi':208.98,'Uuq':290,'O':15.999,'S':32.065,'Se':78.96,
                     'Te':127.60,'Po':209.98,'Uuh':293,'F':18.998,'Cl':35.443,
                     'Br':79.904,'I':126.90,'At':209.99,'Uus':294,'He':4.0026,
                     'Ne':20.180,'Ar':39.948,'Kr':83.798,'Xe':131.29,'Rn':222.02,
                     'UUo':294}

#contains energy of formation of organic compounds from Thauer1977 in KJ/mol
gibbs_energy_of_formation_thauer = {'H2':0,'H+':0,'H+_pH7':-39.8,'H2O':-237.178,'HO-':-157.293,'H2O2':-134.097,
        'O2-':28.9,'C_graphite':0,'CO':-137.15,'CO2_g':-394.359,'CO2_aq':-386.02,
        'H2CO3':-623.16,'HCO3-':-586.85,'CO3_2-':-527.90,'CH4':-50.75,'C2H6':-32.89,
        'C2H4':68.12,'C2H2':209.2,'Methanol':-175.39,'Ethanol':-181.75,'n-Propanol':175.81,
        'iso-Propanol':-185.94,'n-Butanol':-171.84,'Ethylene_glycol':-323.21,
        'Glycerol_liq':-477.06,'Glycerol_aq':-488.52,'Mannitol':-942.61,'Sorbitol':942.70,
        'Formaldehyde_aq':-130.54,'Formaldehyde_g':-112.97,'Acetaldehyde_aq':-139.9,
        'Acetaldehyde_g':-128.91,'Acetone':-161.17,'Formate-':-351.04,'Acetate-':-369.41,
        'Proprionate-':-361.08,'Butyrate-':-352.63,'Valerate-':-344.34,'Caporate-':-335.96,
        'Palmitic_acid':-305.0,'Acrylate-':-286.19,'Crotonate-':-277.4,'Glycollate-':-530.95,
        'Lactate-':-517.81,'beta-Hydroxyproprinate':-518.4,'beta-Hydroxybutyrate':-506.3,
        'Glycerate':-658.1,'D-Gluconate-':-1128.3,'Glyoxylate-':-468.6,'Pyruvate-':-474.68,
        'beta-ketobutyrate-':-493.7,'Oxalate-':-698.44,'Oxalate_2-':-674.04,'Succinic_acid':-746.38,
        'Succinate_2-':-690.23,'Fumatic_acid':-647.14,'Fumarate_2-':-604.21,'L-Malate_2-':-845.08,
        'Oxalacetate_2-':-797.18,'alpha-ketoglutarate':-797.55,'Citrate_3-':-1168.34,
        'Isocitrate_3-':-1161.69,'cis-Aconitate_3-':-922.61,'Glyceraldehyde':-437.65,
        'Dihydroxyacetone':-445.18,'D-Erythrose':-598.3,'D-Ribose':-757.3,'alpha-D-Glucose':-917.22,
        'alpha-D-Galactose':-923.53,'D-Fructose':-915.38,'D-Heptose':-1077,'alpha-Lactose':-1515.24,
        'beta-Lactose':-1570.09,'beta-Maltose':-1497.04,'Sucrose':-1551.85,'Glycogen_perGlc':-662.33,
        'L-Alanine':-371.54,'L-Arginine':-240.2,'L-Asparagine_x_H2O':-763.998,
        'L-Aspartic_acid':-721.3,'L-Aspartate-':-700.4,'L-Cyteine':-339.78,
        'L-Csytine':-666.93,'L_Glutamic_acid':-723.8,'L_Glutamate-':-699.6,
        'L-Glutamine':-529.7,'Glycine':-370.788,'Glycine+':-384.192,'Glycine-':-314.963,
        'L-Leucine':-343.1,'L_isoleucine':-343.9,'L-Methionine':-502.92,'L-Phenylalanine':-207.1,
        'L-Serine':-510.87,'L-Threonine_aq':-514.63,'L-Threonine_c':-550.2,
        'L-Tryptophane':-112.6,'L-Tyrosine':-370.7,'L-Valine':-356.9,'Hypoxanthine':89.5,
        'Guanine':46.99,'Xanthine':-165.85,'Urate-':-325.9,'Uric_acid':-356.8,
        'Urea_c':-196.82,'Urea_aq':-203.76,'Creatine':-264.30,'Creatinine':-28.9,
        'Allantoin':-446.098,'NH2CH2CH2SO3-':-446.098,'CH3NH3+':-40.0,'(CH3)2NH2+':-3.3,
        '(CH3)2NH+':37.2,'Pyridine':177.1,'Benzene':124.5,'Phenol_c':-47.6,'Phenol_c2':-52.1,
        'p-Quinone':-83.7,'p-Hydroquinone':-207.0,'Resorcinol':-209.2,'Pyrocatechol':-210.0,
        'o-Cresol':-37.1,'m-Cresol':-40.54,'p-Cresol':-32.09,'Toluene':114.22,'Benzoic_acid_c':-245.6,
        'Benzoic_acid_c2':-248.5,'o-Hydroxybenzoic_aci':-421.33,'m-Hydroxybenzoic_aci':-422.58,
        'p-Hydroxybenzoic_aci':-423.00,'Benzyl_alcohol_liq':-31.25,'Benzyl_alcohol_liq2':-27.6,
        'N2':0,'NH3':-26.57,'NH4+':-79.37,'NO':86.57,'NO2-':-37.2,'NO3-':-111.34,
        'N2O':104.18,'N2H4':128.03,'ClO2-':17.2,'ClO3-':-3.35,'S_rhombic':0,'S_2-':85.8,
        'SH-':12.05,'SH2_g':-33.56,'SH2_aq':-27.87,'SO3_2-':-86.6,'HSO3-':-527.81,
        'H2SO3':-537.895,'SO2_g':-300.194,'SO2_aq':-300.708,'SO4_2-':-744.63,'SO4_H-':-756.01,
        'S2O3_2-':-513.4,'S2O4_2-':-600.4,'HS2O4-':-614.6,'S3O6_2-':-958.1,'S4O6_2-':-1022.2,
        'H2S2O4':-616.7,'Fe_2+':-78.87,'Fe3+':-4.6
        }

#contains energy of formation of organic compounds as given in Battley1993 in KJ/mol standard conditions
gibbs_energy_of_formation_battley = {
        'Ammonia_aq': -26.57,
        'CO2_g':-394.36,
        'Glycerol_aq':-497.48,
        'H+_aq': -39.96,
        'HPO4_2-_aq':-1089.26,
        'HSO4-_aq':-756.01,
        'HS-_aq':+12.05,
        'OH-_aq':-157.29,
        'N2_g': 0,
        'O2_g':0,
        'O2_aq':16.32,
        'KOH_c':-379.11,
        'K2SO4_c':-1321.43,
        'P4O10_c':-2697.84,
        'Succinic_acid_aq': -746.63,
        'H2O_lq':-237.18
        }