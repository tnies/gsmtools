#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file contains utilities to analyse the elemental composition of metabolites
in a genome-scale model
"""

from .constants import molecular_weights as mw
from .utils import get_biomass_rxn
import numpy as np

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


def get_elements(cpd):
    """
    Rough function that returns list of elemental composition when a elemntal 
    formula is provided.
    
    Input
    -----
    cpd: Chemical formula of a compound [string]
    
    Output
    ------
    Dictionary of elemental composition
    
    """
    atoms = ["C","H","O","N","P","S"]
    elements = []
    for i in atoms:
        try:
            idx = cpd.index(i)
            try:
                int(cpd[idx+1])
                try:
                    int(cpd[idx+2])
                    try:
                        int(cpd[idx+3])
                        elements.append(cpd[idx+1:idx+4])
                    except:
                        elements.append(cpd[idx+1:idx+3])
                except:
                    elements.append(cpd[idx+1])
            except:
                try:
                    cpd[idx+1]
                    if cpd[idx+1] not in atoms:
                        elements.append(0)
                    else:
                        elements.append(1)
                except:
                    elements.append(1)
        except:
            elements.append(0)
    return dict(
            zip(atoms,list(
                    map(int,elements))))


   
def get_molecular_weight(edict):
    """
    Calculate molecular weight of compound
    
    Input
    -----
    edict: Dictionary with elemental composition of compound
    
    Output
    ------
    molecular_compound_weight: Molecular weight of a compound
    
    """
    e_dict = edict.copy()
    e_dict.pop('charge')
    elements = list(e_dict.keys())
    stoichiometries = list(e_dict.values())
    molecular_weights = np.array([mw[i] for i in elements])
    molecular_compound_weight = sum(molecular_weights*stoichiometries)
    return molecular_compound_weight



def get_biomass(model,carbon_normalize=True):
    """
    Get the biomass composition of all biomass functions in a genome-scale model
    
    Input
    -----
    model:              Cobrapy genome-scale model
    carbon_normalize:   Normalize to carbons [boolean]
    
    Output
    -----
    biomass: Dict containing dictionaries of all biomass compositions in a model
    
    """
    biomass = {}
    idx = get_biomass_rxn(model)
    for i in idx:
        b = model.reactions[i].check_mass_balance()
        elemental_composition = {}
        for j in ['C','H','O','N','P','S','charge']:
            try:
                elemental_composition[j] = b[j]
            except:
                elemental_composition[j] = 0
        if carbon_normalize:
            bio = {k:v/elemental_composition["C"] for k,v in elemental_composition.items()}
        else:
            bio = elemental_composition
        biomass[model.reactions[i].id] = bio
    return biomass



def get_e_composition(model):
    """
    Get elemental composition of all metabolites in a model
    
    Input
    -----
    model:              Cobrapy genome-scale model
    
    Output
    -----
    elemental_composition: Dictionary containing elemental dictionaries for each
                           compound in the model.
    
    """
    elemental_composition = {}
    for i in model.metabolites:
        e_comp = i.elements
        if i.charge != None:
            e_comp['charge'] = i.charge
        else:
            e_comp['charge'] = 0
        for j in ['C','H','O','N','P','S']:
            if j not in e_comp.keys():
                e_comp[j] = 0
        elemental_composition[i.id] = e_comp
    return elemental_composition

    
__all__=['get_elements','get_molecular_weight','get_biomass','get_e_composition']