#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains utilities for thermodynamic analysis
"""

import numpy as np
from .constants import gibbs_energy_of_formation_battley as Gf_B

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'


def get_energy_of_reaction(stoichiometries,energies):
    """
    Calculates the energy of reaction
    
    Input
    -----
    stoichiometries: Stoichiometries of reaction [np.array] 
    energies:        Energies of formation [np.array]
   
    Output
    ------
    energy_of_reaction: Energy of reaction [float]
    
    """
    energy_of_reaction = sum(stoichiometries,energies)
    return energy_of_reaction

def get_degree_of_reduction(cpd_elements,N_source='N2',y_C_ratio=False):
    """
    Calculates the degree of reduction of a compound.
    
    Input
    -----
    cpd_elements: Element list of compound [C,H,O,N,P,S,charge]
    N_source:     Either N2 or NH4
    y_C_ratio:    Normalize to one carbon
    
    Output
    ------
    y: Degree of reduction [y]
    
    """
    if N_source == 'N2':
        y = 4*cpd_elements['C'] + cpd_elements['H'] - 2*cpd_elements['O'] + \
        0*cpd_elements['N'] + 5*cpd_elements['P'] + 6*cpd_elements['S'] - \
        cpd_elements['charge'] 
    elif N_source == 'NH4':
        y = 4*cpd_elements['C'] + cpd_elements['H'] - 2*cpd_elements['O'] - \
        3*cpd_elements['N'] + 5*cpd_elements['P'] + 6*cpd_elements['S'] - \
        cpd_elements['charge']
    else:
        raise NameError("%s is not a valid nitrogen source" % N_source)
    if y_C_ratio:
        y/cpd_elements[0]
    return y


def get_energy_of_combustion(y):
    """
    Calculates energy of combustion according to Battley1993. Other formulas
    can be found in Stockar1993 and Minkevich1973 (~26.5kcal/av e- = 110.902 KJ/av e-)
    
    Input
    -----
    y: Degree of reduction [float]
    
    Output
    -----
    dG: Energy of comustion [float]
    
    """
    dG = y*-107.90
    return dG


def get_combustion_stoichiometries_of_biomass(biomass): #TODO: Find formula that describes combustion 
    """
    Assumes follwing combustion reaction /see Battley 1993
    biomass + a O2 + b KOH -> c CO2 + d N2 + e P4O10 + f K2SO4 + g H20 
    
    Input
    -----
    biomass: Biomass dictionary obtained by gsmtools.get_biomass
    
    Output
    -----
    x: Stoichiometries of combustions
    
    """
    biomass = list(biomass.values())
    biomass = biomass[:-1]
    a = np.array([
        [0,0,1,0,0,0,0], #C
        [0,1,0,0,0,0,2], #H
        [2,1,2,0,10,4,1], #O
        [0,0,0,2,0,0,0], #N
        [0,0,0,0,4,0,0], #P
        [0,0,0,0,0,1,0], #S
        [0,1,0,0,0,2,0]]) #K
    b = np.append(biomass,0)
    x = np.linalg.solve(a,b)
    return x

def get_energy_of_formation_of_biomass(biomass,NH4=False):
    """
    Calculates the energy of formation of biomass according to the method
    described by Battley et al.
    
    Input
    -----
    biomass: Biomass dictionary obtained by gsmtools.get_biomass
    NH4: boolean if ammonia is the source of nitrogen
    
    Output
    ------
    stoich: Stoichiometry of combustion
    y:      Degree of reduction
    Gc:     Energy of combustion
    Gf:     Energy of formation
    """
    stoich = get_combustion_stoichiometries_of_biomass(biomass)
    if NH4==False:
        y = get_degree_of_reduction(biomass)
    else:
        y = get_degree_of_reduction(biomass,N_source='NH4')
    Gc = get_energy_of_combustion(y)
    Gf = Gc - stoich[0]*Gf_B['O2_g'] - stoich[1]*Gf_B['KOH_c'] - stoich[2]*Gf_B['CO2_g'] - \
    stoich[3]*Gf_B['N2_g'] - stoich[4]*Gf_B['P4O10_c'] - stoich[5]*Gf_B['K2SO4_c'] - \
    stoich[6]*Gf_B['H2O_lq']
    return (stoich,y,Gc,-Gf)

__all__=['get_energy_of_reaction','get_degree_of_reduction','get_energy_of_combustion',
         'get_energy_of_formation_of_biomass']
