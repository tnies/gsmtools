#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains utilities for analyzing genome scale models and metabolic networks
"""

import numpy as np

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__='GPLv3'
__vesion__='0.1'
__maintainer__='Tim Nies'

def get_exchange_reactions(reaction_list):
    """
    Get indeces of exchange reactions.
    
    Input
    -----
    reaction_list: List of reaction names
    
    Output
    ------
    idx: Indices of exchange reactions
    
    """
    idx = [i for i in range(len(reaction_list)) if reaction_list[i].startswith("EX")]
    return idx



def get_non_zero_fluxes(fluxes):
    """
    Extracts the fluxes that are non zero.
    
    Input
    -----
    fluxes: sol.flux from cobra sol object
    
    Output
    ------
    idx: Indices of nonzero fluxes
    
    """
    idx = [i for i in range(len(fluxes)) if fluxes[i] !=0]
    return idx



def get_non_zero_exchange_flux(fluxes):
    """
    Extracts all nonzero excange fluxes.
    
    Input
    -----
    fluxes: sol.flux from cobra sol object
    
    Output
    ------
    idx: Indices of non zero exchange fluxes
    
    """
    ex_idx = get_exchange_reactions(fluxes.index)
    exchange_fluxes = fluxes[ex_idx]
    nonzero_idx = get_non_zero_fluxes(exchange_fluxes)
    idx = np.array(ex_idx)[nonzero_idx]
    return idx 



def get_biomass_rxn(model):
    """Extracts the biomass reaction
    
    Input
    -----
    model: Cobra model
    
    Output
    ------
    idx: Indices of all biomass functions in the model
    
    """
    idx = [i for i in range(len(model.reactions)) if model.reactions[i].id.startswith("BIOMASS")]
    return idx


__all__=['get_exchange_reactions','get_non_zero_fluxes','get_non_zero_exchange_flux',
         'get_biomass_rxn']