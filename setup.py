from setuptools import setup

setup(name='gsmtools',
      version='0.1',
      decription='utilities for analyzing genome scale models and metabolic networks',
      packages=['gsmtools'],
      zip_safe=False)
