<h1>gsmtools</h1>

*gsmtools* is a bundle of functionalities that I used for my master's thesis "Extendenung MacArhur's Consumer-Resource Models by Invasion Analysis and Constraint-based Models".
These functions are rather naively implemented and I cannot guarantee that they are absolutely correct. Thus, if you want to use them be careful.

Below is a list that contains a short description of the files:

+ **utils.py**: Functions for extracting certain fluxes
+ **thermo_utils.py**: Functions to calculate the degree of reduction, energy of formation for biomass
+ **element_utils.py**: Get elemental composition in a form that can be used for further analysis
+ **constants.py**: Some physical constants I have found in some publications
+ **analysis methods**: Some basic analyses methods.

<h2> Contact </h2>
Quantitative und Theoretische Biologie <br>
Heinrich-Heine-Universität Düsseldorf <br>
Building: 22.07.00.031 <br>
Postbox: ZSL 006 <br>
Universitätsstraße 1 <br>
40225 Düsseldorf <br>

